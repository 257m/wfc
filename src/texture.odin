package main

import rl "vendor:raylib"

Texture :: enum {
	BLANK,
	UP,
	RIGHT,
	DOWN,
	LEFT,
}

texture_names: [Texture]cstring = {
	.BLANK  = "tiles/blank.png",
	.UP  = "tiles/up.png",
	.RIGHT  = "tiles/right.png",
	.DOWN  = "tiles/down.png",
	.LEFT  = "tiles/left.png",
}

textures: [Texture]rl.Texture2D

textures_init :: proc() {
	for i in Texture {
		textures[i] = rl.LoadTexture(texture_names[i]);
	}
}
