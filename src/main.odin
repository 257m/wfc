package main

import "core:time"
import "core:fmt"
import "core:slice"
import "core:strings"
import rl "vendor:raylib"
import rand "core:math/rand"
import "core:thread"

WINDOW_WIDTH  :: 1280
WINDOW_HEIGHT :: 900

DIM :: 16

//TILE_WIDTH  :: (WINDOW_WIDTH/DIM)
//TILE_HEIGHT :: (WINDOW_HEIGHT/DIM)
TILE_WIDTH  :: 50
TILE_HEIGHT :: 50

Direction :: enum {
	UP,
	RIGHT,
	DOWN,
	LEFT,
}

Cell :: struct {
	superposition: bit_set[Texture],
	pos: [2]i32
}

lookup_table := [Texture][Direction]bit_set[Texture]{
	.BLANK = {
		.UP = { .BLANK, .UP },
		.RIGHT = { .BLANK, .RIGHT },
		.DOWN = { .BLANK, .DOWN },
		.LEFT = { .BLANK, .LEFT },
	},
	.UP = {
		.UP = { .DOWN, .RIGHT, .LEFT },
		.RIGHT = { .LEFT, .UP, .DOWN },
		.DOWN = { .BLANK, .DOWN },
		.LEFT = { .RIGHT, .UP, .DOWN },
	},
	.RIGHT = {
		.UP = { .DOWN, .RIGHT, .LEFT },
		.RIGHT = { .LEFT, .UP, .DOWN },
		.DOWN = { .UP, .RIGHT, .LEFT },
		.LEFT = { .BLANK, .LEFT },
	},
	.DOWN = {
		.UP = { .BLANK, .UP },
		.RIGHT = { .LEFT, .UP, .DOWN },
		.DOWN = { .UP, .RIGHT, .LEFT },
		.LEFT = { .RIGHT, .UP, .DOWN },
	},
	.LEFT = {
		.UP = { .DOWN, .RIGHT, .LEFT },
		.RIGHT = { .BLANK, .RIGHT },
		.DOWN = { .UP, .RIGHT, .LEFT },
		.LEFT = { .RIGHT, .UP, .DOWN },
	},
}

grid: [DIM*DIM]Cell
gridCopyArray: [DIM*DIM]^Cell
tiles := gridCopyArray[:]

sb := strings.builder_make()

main :: proc() {
	rl.InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Wolomon")
	defer rl.CloseWindow()   

	
	rl.SetTargetFPS(60)

	textures_init()
	for j in 0..<i32(DIM) {
		for i in 0..<i32(DIM) {
			grid[j*DIM+i] = Cell{ superposition = { .BLANK, .UP, .RIGHT, .DOWN, .LEFT }, pos = {i, j}}
		}
	}

	for _, i in grid {
		tiles[i] = &grid[i]
	}

	t := thread.create_and_start(update_loop)

	for !rl.WindowShouldClose() { // Detect window close button or ESC key
		rl.BeginDrawing()
		defer rl.EndDrawing()
		render()
	}
}

update_loop :: proc() {
	using time
	for {
		s: Stopwatch
		stopwatch_start(&s)
		if update(&tiles) {
			fmt.println(stopwatch_duration(s))
			fmt.println("Done!")
			for {
				time.sleep(1 * Hour)
			}
		}
		fmt.println(stopwatch_duration(s))
	}
}

pick_from_slice :: proc(array: []$T) -> T {
	return array[rand.int_max(len(array))]
}

pick_from_bs :: proc(bs: bit_set[$T]) -> T {
	if card(bs) <= 1 {
		return pick_first(bs)
	}
	r := rand.int_max(card(bs))
	k := 0
	for i in bs {
		if k == r {
			return i
		}
		k += 1
	}
	return T(r)
}

pick_first :: proc(bs: bit_set[$T]) -> T {
	for i in bs do return i
	return .BLANK
} 

neighbour_superposition :: proc(c: ^Cell, d: Direction) -> bit_set[Texture] {
	if (card(c.superposition) < 1) {
		return auto_cast {}
	}
	possible := lookup_table[pick_first(c.superposition)][d]
	for i in c.superposition {
		possible += lookup_table[i][d]
	}
	return possible
}

cell_update :: proc(c: ^Cell) -> bool {
	// Collapse possibilities as far as possible
	if c.pos.y > 0 {
		c.superposition &= neighbour_superposition(&grid[(c.pos.y-1)*DIM+c.pos.x], .DOWN)
	}
	if c.pos.y < DIM-1 {
		c.superposition &= neighbour_superposition(&grid[(c.pos.y+1)*DIM+c.pos.x], .UP)
	}
	if c.pos.x > 0 {
		c.superposition &= neighbour_superposition(&grid[c.pos.y*DIM+(c.pos.x-1)], .RIGHT)
	}
	if c.pos.x < DIM-1 {
		c.superposition &= neighbour_superposition(&grid[c.pos.y*DIM+(c.pos.x+1)], .LEFT)
	}
	return card(c.superposition) > 0
}

cell_update_safe :: proc(c: ^Cell) -> bool {
	old_superposition := c.superposition
	cell_update(c)
	if card(c.superposition) < 0 {
		c.superposition = old_superposition
		return false
	}
	return true
}

cell_cmp :: proc(i, j: ^Cell) -> slice.Ordering {
	entropyi := card(i.superposition)
	entropyj := card(j.superposition)

	if entropyi == entropyj {
		return .Equal
	}
	if entropyi > entropyj {
		return .Greater
	}
	return .Less
}

heuristic_picks :: proc(t: ^[]^Cell) -> []^Cell {
	// Take out all collapsed cells from (t^)
	entropy := card((t^)[0].superposition)
	for _, i in (t^) {
		if card((t^)[i].superposition) > entropy {
			entropy = card((t^)[i].superposition)
			(t^) = (t^)[i:]
			// Pick a random cell that has the same entropy
			for _, i in (t^) {
				if card((t^)[i].superposition) > entropy {
					return (t^)[:i-1]
				}
			}
			return (t^)
		}
	}
	picks := (t^)
	(t^) = (t^)[1:]
	return picks
}

update :: proc(t: ^[]^Cell) -> bool {
	slice.sort_by_cmp(t^, cell_cmp)
	picks := heuristic_picks(t)
	if len(t^) < 1 {
		fmt.println(len(t^))
		return true
	}
	last_grid := grid
	next_tiles := (t^)
	using time
	s: Stopwatch
	stopwatch_start(&s)
	update_loop : for {
		if stopwatch_duration(s) > (250 * Millisecond) {
			return false
		}
		// Pick cell with least entropy
		if len(picks) < 1 {
			return false
		}
		cell := picks[0]
		picks = picks[1:]

		// Update the cells superpositions to reflect neighbours
		old_superposition := cell.superposition
		if !cell_update(cell) {
			cell.superposition = old_superposition
			continue update_loop
		}

		// Collapse into one of its possible states
		cell.superposition = {pick_from_bs(cell.superposition)}
		for _ in 0..<2 do if !validate() {
			grid = last_grid
			next_tiles = t^
			continue update_loop
		}
		
		time.sleep(10 * Millisecond)
		if !update(&next_tiles) {
			grid = last_grid
			next_tiles = t^
			continue update_loop
		}
		return true
	}
}

validate :: proc() -> bool {
	for j in 0..<i32(DIM) {
		for i in 0..<i32(DIM) {
			if !cell_update(&grid[j*DIM+i]) do return false
		}
	}
	return true
}

render :: proc() {
	for j in 0..<i32(DIM) {
		for i in 0..<i32(DIM) {
			if card(grid[j*DIM+i].superposition) == 1 {
				rl.DrawTexture(textures[pick_first(grid[j*DIM+i].superposition)], i * TILE_WIDTH, j * TILE_HEIGHT, rl.WHITE)
			}
			else {
				rl.DrawRectangle(i * TILE_WIDTH, j * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT, rl.BLACK)
				rl.DrawRectangleLines(i * TILE_WIDTH, j * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT, rl.WHITE)
				strings.write_int(&sb, card(grid[j*DIM+i].superposition));
				rl.DrawText(strings.clone_to_cstring(strings.to_string(sb)), i * TILE_WIDTH + TILE_WIDTH/3, j * TILE_HEIGHT + TILE_HEIGHT/3, 20, rl.WHITE)
				strings.builder_reset(&sb)
			}
		}
	}

	rl.ClearBackground(rl.WHITE)
}
